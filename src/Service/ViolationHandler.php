<?php


namespace App\Service;


use App\Exception\CustomValidationException;

class ViolationHandler
{
	/**
	 * @param $violations
	 * @throws CustomValidationException
	 */
	public function retrieve($violations)
	{

		$errors = [];
		$message = $violations->count()." Violations found" ;
		$i = 1;
		foreach ($violations as $violation) {
			$errors['Violation #'.$i] =
				[
					"Field" => $violation->getPropertyPath(),
					"Description" => $violation->getMessage()
				];
			++$i;
		}
		throw new CustomValidationException($message,400, null, $errors);
	}

}