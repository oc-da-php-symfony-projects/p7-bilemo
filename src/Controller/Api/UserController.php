<?php

namespace App\Controller\Api;

use App\Entity\Company;
use App\Entity\User;
use App\Exception\CustomHttpException;
use App\Exception\CustomValidationException;
use App\Repository\UserRepository;
use App\Representation\PagerRepresentation;
use App\Service\ViolationHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Areas;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class UserController
 * @package App\Controller\Api
 */
class UserController extends AbstractFOSRestController
{
	private UserRepository $repository;

	public function __construct(UserRepository $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @Areas({"bilmo"})
	 * @OA\Tag(name="Bilmo Admin")
	 * @Operation(operationId="getUsers")
	 * @OA\Get(path="/bilmo/users",description="Get users list for super admin",operationId="getUsers")
	 * @OA\Parameter(name="page",in="query",description="The Page number",@OA\Schema(type="integer")),
	 * @OA\Parameter(name="sort",in="query", description="The User property to order by",@OA\Schema(type="string")),
	 * @OA\Parameter(name="order",in="query",description="The sorting direction",@OA\Schema(type="string")),
	 * @OA\Parameter(name="search",in="query",description="The search term", @OA\Schema(type="string"))
	 * @OA\Response(response=200, description="Ok",
	 *      content={@OA\MediaType
	 *                  (mediaType="application/json",
	 *                      @OA\Schema(type="object",
	 *                          @OA\Property(property="metas", type="object",description="Informations about search result",
	 *                          example={
	 *                          "Total Records": "integer",
	 *                          "Total Search Hits": "integer",
	 *                          "Records per page": "integer",
	 *                          "Total Pages": "integer",
	 *                          "Current page": "integer",
	 *                          "Next": "string"
	 *                          }),
	 *                          @OA\Property(property="users", type="array",description="Users datas", @OA\Items(ref=@Model(type=User::class, groups={"userlist","company"})))
	 *                  ))
	 *      }
	 * )
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status", type="string",description="status code"),
	 *                     @OA\Property(property="message", type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                ))}
	 * )
	 * @Rest\Get ("/bilmo/users", name="users")
	 * @Rest\View (serializerGroups={"superadmin","userlist","company"})
	 * @QueryParam(name="page",requirements="\d+",default="1",description="Page number for pagination")
	 * @QueryParam(name="sort", requirements="lastname|company|id|role",default="lastname",description="Order by property")
	 * @QueryParam(name="order",requirements="asc|desc",default="asc", description="Sort order (asc, desc)")
	 * @QueryParam(name="search", requirements="[a-zA-Z]+", default=null,nullable=true,description="Search query to look for user")
	 * @IsGranted("ROLE_SUPER_ADMIN", message="You don't have proper access level authorization.")
	 * @param ParamFetcherInterface $paramFetcher
	 * @param CacheInterface $cache
	 * @param Request $request
	 * @return View
	 * @throws InvalidArgumentException
	 */
	public function users(ParamFetcherInterface $paramFetcher, CacheInterface $cache, Request $request): View
	{
		$page = max(1,$paramFetcher->get('page', 1));
		$users = $cache->get('users'.$page,function (ItemInterface $item) use ($page, $paramFetcher) {
			$item->expiresAfter(15);
			return $this->repository->GetPaginatedUsers(strtolower($paramFetcher->get('search')), $paramFetcher->get('sort'), $paramFetcher->get('order'), $page);
		},1.0);
		return $this->view((new PagerRepresentation($users, $request->getSchemeAndHttpHost()))->getRepresentation());
	}

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Company Users")
	 * @Operation(operationId="getCompanyUsers")
	 * @OA\Get(path="/companies/{company}/users" ,description="Get company users list for company admin or super admin", operationId="getCompanyUsers")
	 *     @OA\Parameter(name="company",in="path",description="The Company Id",@OA\Schema(type="integer")),
	 *     @OA\Parameter(name="page", in="query",description="The Page number", @OA\Schema(type="integer")),
	 *     @OA\Parameter(name="sort",in="query",description="The User property to order by", @OA\Schema(type="string")),
	 *     @OA\Parameter(name="order", in="query",description="The sorting direction",@OA\Schema(type="string"))
     * @OA\Response(response=200, description="Ok",
	 *      content={@OA\MediaType
	 *                  (mediaType="application/json",
	 *                      @OA\Schema(type="object",
	 *                          @OA\Property(property="metas", type="object",description="Informations about search result",
	 *                          example={
	 *                          "Total Records": "integer",
	 *                          "Total Search Hits": "integer",
	 *                          "Records per page": "integer",
	 *                          "Total Pages": "integer",
	 *                          "Current page": "integer",
	 *                          "Next": "string"
	 *                          }),
	 *                          @OA\Property(property="users", type="array",description="Users datas", @OA\Items(ref=@Model(type=User::class, groups={"userlist","company"})))
	 *                  ))
	 *      }
	 * )
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status", type="string",description="status code"),
	 *                     @OA\Property(property="message", type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                ))}
	 * )
	 * @Rest\Get ("/companies/{company}/users", name="company_users", requirements={"company"="\d+"})
	 * @Rest\View (serializerGroups={"superadmin","userlist","company"})
	 * @QueryParam(name="page",requirements="\d+",default=1,description="Page number for pagination")
	 * @QueryParam(name="sort",requirements="lastname|role|id", default="lastname",description="Order by property")
	 * @QueryParam(name="order",requirements="asc|desc", default="asc",description="Sort order (asc, desc)")
	 * @QueryParam(name="search",requirements="[a-zA-Z]+",default=null,nullable=true,description="Search query to look for user")
	 * @IsGranted("ROLE_ADMIN", message="You don't have proper access level authorization.")
	 * @param Company $company
	 * @param ParamFetcherInterface $paramFetcher
	 * @param CacheInterface $cache
	 * @return View
	 * @throws InvalidArgumentException
	 */
	public function companyUsers(Company $company, ParamFetcherInterface $paramFetcher,CacheInterface $cache, Request $request): View
	{
		$this->access($company);
		$page = max(1,$paramFetcher->get('page', 1));
		$users = $cache->get('company-users'.$company->getId().$page, function (ItemInterface $item) use ($paramFetcher, $company, $page) {
			$item->expiresAfter(15);
			return $this->repository->GetPaginatedCompanyUsers($company, $paramFetcher->get('search'), $paramFetcher->get('sort'), $paramFetcher->get('order'), $page);
		},1.0);
		return $this->view((new PagerRepresentation($users, $request->getSchemeAndHttpHost()))->getRepresentation()) ;
	}

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Company Users")
	 * @Operation(operationId="getCompanyUser")
	 * @OA\Get(path="/companies/{company}/users/{user}",description="Get user by company and userId",operationId="getCompanyUser")
	 *  @OA\Parameter(name="company", in="path", description="The Company Id", @OA\Schema(type="integer")),
	 *  @OA\Parameter(name="user",in="path",description="The User Id",@OA\Schema(type="integer"))
	 * @OA\Response(
	 *   response=200, description="Ok",
	 *   @OA\JsonContent(ref=@Model(type=User::class, groups={"usershow"}))
	 * )
	 * @OA\Response(response=404, description="Not Found",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property( property="status", type="string",description="status code" ),
	 *                     @OA\Property(property="error", type="string",description="violation description"),
	 *                     example={"status": "404","lastname": "We couldn't find this User"}
	 *                   ))}
	 * )
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType( mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property( property="status",type="string",description="status code"),
	 *                     @OA\Property(property="message",type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                   ))}
	 * )
	 * @Rest\Get ("/companies/{company}/users/{user}", name="company_user", requirements={"company"="\d+","user"="\d+"})
	 * @Rest\View (serializerGroups={"superadmin","usershow","company"})
	 * @IsGranted("ROLE_ADMIN", message="You don't have proper access level authorization.")
	 * @Cache(lastModified="user.getUpdatedAt()", Etag="'User' ~ user.getId() ~ user.getUpdatedAt().getTimestamp()")
	 * @param Company $company
	 * @param User|null $user
	 * @param CacheInterface $cache
	 * @return User|null
	 * @throws InvalidArgumentException
	 */
	public function companyUser(Company $company, ?User $user, CacheInterface $cache): ?User
	{
		$this->access($company);
		$this->check($company,$user);
		return $cache->get('company-user'.$company->getId().$user->getId(), function (ItemInterface $item) use ($user) {
			$item->expiresAfter(15);
			return $user;
		},1.0);
	}

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Company Users")
	 * @Operation(operationId="updateCompanyUser")
	 * @OA\Put(path="/companies/{company}/users/{user}",description="Update company user by userId",operationId="updateCompanyUser")
	 *  @OA\Parameter(name="company", in="path", description="The Company Id", @OA\Schema(type="integer")),
	 *  @OA\Parameter(name="user",in="path",description="The User Id",@OA\Schema(type="integer")))
	 * @OA\Response(response=404, description="Not Found",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property( property="status", type="string",description="status code" ),
	 *                     @OA\Property(property="error", type="string",description="violation description"),
	 *                     example={"status": "404","lastname": "We couldn't find this User"}
	 *                   ))}
	 * )
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType( mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property( property="status",type="string",description="status code"),
	 *                     @OA\Property(property="message",type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                   ))}
	 * )
	 * @OA\Response(response=200,description="OK",
	 *     @OA\JsonContent(ref=@Model(type=User::class, groups={"usershow"}),
	 *     @OA\Link(link="companyUser"))
	 * )
	 * @Rest\Put("/companies/{company}/users/{user}", name="user_update", requirements={"company"="\d+","user"="\d+"})
	 * @Rest\View(statusCode = 200,serializerGroups={"admin","usershow"})
	 * @IsGranted("ROLE_ADMIN", message="You don't have proper access level authorization.")
	 * @param Company $company
	 * @param User|null $user
	 * @param Request $request
	 * @param EntityManagerInterface $entityManager
	 * @param ValidatorInterface $validator
	 * @param ViolationHandler $violationHandler
	 * @return View
	 * @throws CustomValidationException
	 */
	public function update(
		Company $company,
		?User $user,
		Request $request,
		EntityManagerInterface $entityManager,
		ValidatorInterface $validator,
		ViolationHandler $violationHandler
	): View
	{
		$this->access($company);
		$this->check($company,$user);
		$data = json_decode($request->getContent());
		foreach ($data as $key => $value){
			if($key && !empty($value)) {
				$method = 'set'.ucfirst($key);
				$user->$method($value);
			}
		}
		$user->setUpdatedAt(new DateTime('now'));
		$violations = $validator->validate($user, null, ['update']);
		if(count($violations)) {
			$violationHandler->retrieve($violations);
		}

		$entityManager->flush();

		return $this->view($user, Response::HTTP_OK, ['Location' => $this->generateUrl('company_user', ['company' =>$company->getId(),'user' => $user->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
	}

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Company Users")
	 * @Operation(operationId="deleteCompanyUser")
	 * @OA\Delete(path="/companies/{company}/users/{user}",description="Delete company user by userId",operationId="deleteCompanyUser")
	 *  @OA\Parameter(name="company", in="path", description="The Company Id", @OA\Schema(type="integer")),
	 *  @OA\Parameter(name="user",in="path",description="The User Id",@OA\Schema(type="integer")))
	 * @OA\Response(
	 *   response=204, description="No content"
	 * )
	 *	@OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=404, description="Not Found",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property( property="status", type="string",description="status code" ),
	 *                     @OA\Property(property="error", type="string",description="violation description"),
	 *                     example={"status": "404","lastname": "We couldn't find this User"}
	 *                   ))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType( mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property( property="status",type="string",description="status code"),
	 *                     @OA\Property(property="message",type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                   ))}
	 * )
	 * @Rest\Delete("/companies/{company}/users/{user}", name="user_delete", requirements={"company"="\d+","user"="\d+"})
	 * @IsGranted("ROLE_ADMIN", message="You don't have proper access level authorization.")
	 * @param Company $company
	 * @param User|null $user
	 * @param EntityManagerInterface $entityManager
	 * @return Response
	 */
	public function delete(Company $company, ?User $user, EntityManagerInterface $entityManager): Response
	{
		$this->access($company);
		$this->check($company,$user);
		$entityManager->remove($user);
		$entityManager->flush();
		return new Response(null, Response::HTTP_NO_CONTENT);
	}

	private function access(Company $company): bool
	{
		if ($company !== $this->getUser()->getCompany() & !$this->isGranted('ROLE_SUPER_ADMIN')){
			throw $this->createAccessDeniedException('You don\'t belong to this Company');
		}
		return true;
	}

	private function check(Company $company, ?User $user)
	{
		if (null === $user){
			throw new CustomHttpException(Response::HTTP_NOT_FOUND,'User doesn\'t exists',null, [
				'Content-Type' => 'application/json'
			],404);
		}
		if ($user->getCompany() !== $company){
			throw new CustomHttpException(Response::HTTP_BAD_REQUEST, 'This user does not belong to this Company', null, [
				'Content-Type' => 'application/json'
			], 400);
		}
	}

}
