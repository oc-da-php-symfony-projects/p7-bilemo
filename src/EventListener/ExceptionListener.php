<?php


namespace App\EventListener;


use App\Exception\CustomHttpException;
use App\Exception\CustomValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;


class ExceptionListener implements EventSubscriberInterface
{

	public function onKernelException(ExceptionEvent $event)
	{
		// Getting the exception object from event
		$exception = $event->getThrowable();
		$message = [];

		if (!$exception instanceof CustomHttpException & !$exception instanceof CustomValidationException) {
//			dd($exception);
			$code = method_exists($exception,'getStatusCode') ? $exception->getStatusCode() : 500;
			$message = [
				'Error status' => $code ,
				'Description: ' => $exception->getMessage()
			];
		}

		if ($exception instanceof CustomHttpException) {
			$message = [
				'Error status' => $exception->getStatusCode(),
				'Description: ' => $exception->getCustomMessage()
			];
		}
		if ($exception instanceof CustomValidationException) {
			$message = [
				'Error status:' => $exception->getCode(),
				$exception->getMessage() =>	$exception->getOptions()
			];
		}

		// Customizing response object to display other exceptions details
		$response = (new JsonResponse($message));
		$response->setStatusCode(method_exists($exception,'getStatusCode') ? $exception->getStatusCode() : 500);
		// sends the modified response object to the event
		$event->setResponse($response);
	}

	/**
	 * @inheritDoc
	 */
	public static function getSubscribedEvents()
	{
		return [
			KernelEvents::EXCEPTION => 'onKernelException'
		];
	}
}
