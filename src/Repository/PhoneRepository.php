<?php

namespace App\Repository;

use App\Entity\Phone;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Phone|null find($id, $lockMode = null, $lockVersion = null)
 * @method Phone|null findOneBy(array $criteria, array $orderBy = null)
 * @method Phone[]    findAll()
 * @method Phone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneRepository extends AbstractRepository
{
	private int $limit;

	public function __construct(ManagerRegistry $registry, int $limit)
	{
		parent::__construct($registry, Phone::class);
		$this->limit = $limit;
	}

	public function GetPaginatedPhones($term, $sort , $order, $page)
	{
		$query = $this->createQueryBuilder('p')
			->setFirstResult(($page - 1) * $this->limit)
			->setMaxResults($this->limit)
			->orderBy('p.'.$sort, $order = $order ? :'ASC')
		;
		if ($term) {
			$query->where('p.name LIKE ?1')
				->setParameter(1, '%'.$term.'%')
			;
		}

		$query->getQuery();

		return $this->paginate($query,"phones",$page,$this->limit,count($this->findAll()));
	}

}
