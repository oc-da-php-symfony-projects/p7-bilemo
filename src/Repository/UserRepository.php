<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends AbstractRepository implements PasswordUpgraderInterface
{
	private int $limit;

    public function __construct(ManagerRegistry $registry, int $limit)
    {
        parent::__construct($registry, User::class);
	    $this->limit = $limit;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

	public function GetPaginatedUsers($term, $sort, $order,$page)
	{
		$query = $this->createQueryBuilder('u')
			->setFirstResult(($page - 1) * $this->limit)
			->setMaxResults($this->limit)
			->orderBy('u.'.$sort, $order = $order?$order:'ASC');
		if ($term) {

			$query->where('u.lastname LIKE ?1')
				->orWhere('u.firstname LIKE ?1')
				->orWhere('u.company LIKE ?1')
				->setParameter(1, '%'.$term.'%')
			;
		}
		$query->getQuery();

		return $this->paginate($query,"users",$page,$this->limit,count($this->findAll()));
	}

	public function GetPaginatedCompanyUsers($company,$term, $sort, $order, $page)
	{
		$query = $this->createQueryBuilder('u')
			->setFirstResult(($page - 1) * $this->limit)
			->setMaxResults($this->limit)
			->orderBy('u.'.$sort, $order = $order?$order:'ASC')
			->andWhere('u.company = :company')
			->setParameter('company', $company);
		if ($term) {
			$query->andWhere('u.lastname LIKE :user')
				->setParameter('user', '%'.$term.'%')
			;
		}
		$query->getQuery()->expireQueryCache();

		return $this->paginate($query,"users",$page,$this->limit,count($this->findBy(['company'=>$company])));
	}

}
