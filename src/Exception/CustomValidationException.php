<?php

namespace App\Exception;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

class CustomValidationException extends Exception implements HttpExceptionInterface
{
	protected array $headers;
	protected array $options;

	public function __construct(?string $message, ?int $code, ?Throwable $previous, array $options)
	{
		parent::__construct($message, $code, $previous);
		$this->headers = [
			'Content-Type' => 'application/json'
		];
		$this->options = $options;
	}

	public function getStatusCode(): int
	{
		return $this->getCode();
	}

	public function getHeaders(): array
	{
		return $this->headers;
	}

	public function getOptions(): array
	{
		return $this->options;
	}

}