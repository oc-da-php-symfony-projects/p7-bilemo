<?php


namespace App\Representation;

use LogicException;


/**
 * Class PagerRepresentation
 * @package App\Representation
 */
class PagerRepresentation
{

	private array $data;

	private array $meta;

	private array  $pager;

	private string $url;

	public function __construct($pager, $url)
	{

		$this->pager = $pager;
		$this->url =$url;

		$this->makeRepresentation();
	}

	public function addMeta($name, $value)
	{
		if (isset($this->meta[$name])) {
			throw new LogicException(sprintf('This meta already exists, use the setMeta method instead for the %s meta.', $name));
		}
		$this->setMeta($name, $value);
	}

	public function setMeta($name, $value)
	{
		$this->meta[$name] = $value;
	}

	public function getMeta(): array
	{
		return $this->meta;
	}

	public function getData(): array
	{
		return $this->data;
	}

	public function makeRepresentation()
	{
		$this->data = $this->pager['resources'];
		if (count($this->data) === 0 ){
			$this->data = ["No Records found"];
			$this->addMeta('Total Search Records',0);
		}
		elseif ($this->pager['totalQuery'] <= $this->pager['limit'])
		{
			$this->addMeta('Total Records',$this->pager['totalItem']);
			$this->addMeta('Total Search Hit(s)', $this->pager['totalQuery']);
			$this->addMeta('Total Pages', 1);
		}
		else{
			$next = $this->pager['page'] + 1;
			$previous = $this->pager['page'] - 1 ;
			$url = $this->url.'/api/v1/'.$this->pager['resources_name'].'?page=';
			$this->addMeta('Total Records',$this->pager['totalItem']);
			$this->addMeta('Total Search Hits',$this->pager['totalQuery']);
			$this->addMeta('Records per page', $this->pager['limit']);
			$this->addMeta('Total Pages', (int)ceil(($this->pager['totalItem'] / $this->pager['limit'])));
			$this->addMeta('Current page',$this->pager['page']);
			if ($previous > 0) {
				$this->addMeta('Previous', $url . $previous);
			}
			if ($next <= $this->meta['Total Pages']) {
				$this->addMeta('Next', $url . $next );
			}
		}
	}

	public function getRepresentation(): array
	{
		return [
			"meta"=>$this->meta,
			$this->pager['resources_name'] => $this->data
		];
	}
}