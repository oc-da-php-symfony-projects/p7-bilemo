<?php

namespace App\DataFixtures;

use App\Entity\Phone;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PhoneFixtures extends Fixture implements FixtureGroupInterface
{

	public function load(ObjectManager $manager)
	{
		$faker = Factory::create();
		$nms = [];
		$names = [];
		for ($i = 0; $i < 4; $i++) { $nms[] = $faker->regexify('BILEMO-[A-Z0-9]{5}');}
		foreach ($nms as $name) {
			for ($i = 1; $i < 6; $i++) { $names[] = $name . ' ' . $i;}
		}

		foreach ($names as $name) {
			$phone = new Phone();
			$phone->setName($name)
			->setWeight(rand(250, 400))
			->setPrice(rand(500, 1000))
			->setDescription('The best of Bilemo just for your customers')
			->setUpdatedAt(new DateTime());
			$manager->persist($phone);
		}
			$manager->flush();
	}

	public static function getGroups(): array
	{
		return ['PhoneFixture'];
	}
}
