<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * @Serializer\ExclusionPolicy("all")
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "company",
 *          parameters = { "company" = "expr(object.getId())" },
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"companieslist","company","superadmin"}))
 * @Hateoas\Relation(
 *      "companies",
 *      href = @Hateoas\Route(
 *          "companies",
 *          absolute=true
 *      ),
 *     exclusion = @Hateoas\Exclusion(groups={"superadmin","companieslist","company"}, excludeIf = "expr(not is_granted('ROLE_SUPER_ADMIN'))"
 *      )
 * )
 *  @Hateoas\Relation(
 *      "company users",
 *      href = @Hateoas\Route(
 *          "company_users",
 *          parameters = { "company" = "expr(object.getId())" },
 *          absolute=true
 *      ),
 *     exclusion = @Hateoas\Exclusion(groups={"superadmin","companieslist","company"}, excludeIf = "expr(not is_granted('ROLE_ADMIN'))"
 *      )
 * )
 * @OA\Schema(
 *     description="Company",
 *     type="object",
 *     title="Company"
 * )
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"company","companieslist"})
     * @OA\Property(type="int64",example="1")
     */
	private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     */
	private ?string $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"company","companieslist"})
     * @OA\Property(type="string",maxLength=255)
     */
	private ?string $name;

	/**
	 * @Serializer\Groups("company_users")
	 * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EAGER")
	 */
	private ?Collection $users;

    /**
     * @ORM\Column(type="datetime")
     */
	private ?DateTimeInterface $createdAt;

	/**
	 * @ORM\Column(type="datetime")
	 * @OA\Property(type="datetime")
	 */
	private ?DateTimeInterface $updatedAt;

	public function __construct()
   	{
   		$this->createdAt = new DateTime();
   		$this->updatedAt = new DateTime();
   		$this->uuid = $this->generateToken();
   		$this->users = new ArrayCollection();
   	}

	/**
	 * @param int $length
	 * @return bool|string
	 */
	private function generateToken($length = 128)
   	{
   		$hash = "";
   		while(strlen($hash) < $length){
   			try {
   				$hash .= hash('sha512', random_bytes($length));
   			} catch (Exception $e) {
   				$hash .= hash('sha512', mt_rand(0,$length*10000));
   			}
   		}
   		return substr($hash, 0,$length);
   	}

	public function regenerateUuid()
   	{
   		$this->uuid = $this->generateToken();
   	}


    public function getId(): int
    {
        return $this->id;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Company
    {
        $this->name = $name;

        return $this;
    }

	/**
	 * @return ArrayCollection
	 */
	public function getUsers(): Collection
   	{
   		return $this->users;
   	}

	/**
	 * @param User $user
	 * @return Company
	 */
	public function addUser(User $user): self
   	{
   		if (!$this->users->contains($user)){
   			$this->users->add($user);
   		}
   		return $this;
   	}

	/**
	 * @param User $user
	 * @return Company
	 */
	public function removeUser(User $user): self
   	{
   		if ($this->users->contains($user)){
   			$this->users->remove($user);
   		}
   		return $this;
   	}



    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

	/**
	 * @return DateTimeInterface
	 */
	public function getUpdatedAt(): DateTimeInterface
	{
		return $this->updatedAt;
	}

	/**
	 * @param DateTimeInterface $updatedAt
	 * @return Company
	 */
	public function setUpdatedAt(DateTimeInterface $updatedAt): self
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}


}
