<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"},errorPath="email", groups={"registration","update"},message="The email {{ value }} is not available")
 * @UniqueEntity(fields={"username"},errorPath="username", groups={"registration","update"},message="The username {{ value }} is not available")
 * @Serializer\ExclusionPolicy("all")
 * @Hateoas\Relation(
 *     "company",
 *     embedded = @Hateoas\Embedded("expr(object.getCompany())"),
 *     exclusion= @Hateoas\Exclusion(groups={"company","usershow","userlist","superadmin"})
 * )
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "company_user",
 *          parameters = { "user" = "expr(object.getId())","company" ="expr(object.getCompany().getId())" },
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"usershow","userlist"})
 * )
 *
 *  @Hateoas\Relation(
 *     "put",
 *     href = @Hateoas\Route(
 *          "company_user",
 *          parameters = { "user" = "expr(object.getId())","company" ="expr(object.getCompany().getId())" },
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"usershow","userlist"}, excludeIf = "expr(not is_granted('ROLE_ADMIN'))")
 *      )
 * )
 *  @Hateoas\Relation(
 *     "delete",
 *     href = @Hateoas\Route(
 *          "company_user",
 *          parameters = { "user" = "expr(object.getId())","company" ="expr(object.getCompany().getId())" },
 *          absolute = true,
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"usershow","userlist"}, excludeIf = "expr(not is_granted('ROLE_ADMIN'))")
 * )
 * @Hateoas\Relation(
 *     "post",
 *     href = @Hateoas\Route(
 *          "register_user",
 *          parameters = {"company" ="expr(object.getCompany().getId())" },
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"usershow","userlist"}, excludeIf = "expr(not is_granted('ROLE_ADMIN'))")
 *      )
 * )
 *  @Hateoas\Relation(
 *     "company users",
 *     href = @Hateoas\Route(
 *          "company_users",
 *          parameters = { "company" ="expr(object.getCompany().getId())" },
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"usershow","userlist"}, excludeIf = "expr(not is_granted('ROLE_ADMIN'))")
 * )
 *  @Hateoas\Relation(
 *     "users",
 *     href = @Hateoas\Route(
 *          "users",
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"superadmin"}, excludeIf = "expr(not is_granted('ROLE_SUPER_ADMIN'))")
 * )
 *  @OA\Schema(
 *     description="User",
 *     type="object",
 *     title="User"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"userlist","usershow"})
     * @OA\Property(type="int64",example="1")
     */
	private ?int $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Expose()
	 * @Serializer\Type ("string",)
	 */
	private ?string $uuid;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Company" , inversedBy="users", fetch="EAGER")
	 * @OA\Property(type="int64",example="1")
	 * @Serializer\Expose()
	 */
	private ?Company $company;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Expose()
	 * @Serializer\Groups({"userlist", "usershow"})
	 * @Assert\NotBlank(groups={"registration"}))
	 * @Assert\Length(min="2", max="255",groups={"registration"})
	 * @Assert\Length(min="0", max="255",groups={"update"})
	 * @Assert\Regex(
	 *     pattern="/\d/",
	 *     match=false,
	 *     message="Your name cannot contain a number, {{ value }} is not a valid.",
	 *     groups={"registration","update"})
	 * @OA\Property(type="string", maxLength=255)
	 */
	private ?string $firstname;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Groups({"userlist", "usershow"})
	 * @Serializer\Expose()
	 * @Assert\NotBlank(groups={"registration"}))
	 * @Assert\Length(min="2", max="255",groups={"registration"})
	 * @Assert\Length(min="0", max="255",groups={"update"})
	 * @Assert\Regex("/[a-zA-Z]/",groups={"registration","update"},message="The name {{ value }} is not a valid name")
	 * @OA\Property(type="string", maxLength=255)
	 */
	private ?string $lastname;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Groups({"usershow"})
	 * @Serializer\Expose()
	 * @Assert\NotBlank(groups={"registration"})
	 * @Assert\Length(min="2", max="255",groups={"registration"})
	 * @Assert\Length(min="0", max="255",groups={"update"})
	 * @Assert\Email (groups={"registration","update"},message="{{ value }} is not valid for this field. Enter your email")
	 * @OA\Property(type="string", maxLength=255)
	 */
	private ?string $username;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"usershow"})
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\Email(
     *     groups={"registration","update"},
     *     message = "The email {{ value }} is not a valid email."
     * )
     * @OA\Property(type="string", maxLength=255)
     */
	private ?string $email;

    /**
     * @ORM\Column(type="json")
     * @Serializer\Expose()
     * @Serializer\Groups({"superadmin"})
     * @Serializer\Type ("array")
     * @OA\Property(type="array")
     */
	private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     * @Assert\NotBlank(groups={"registration"}))
     * @OA\Property(type="string", maxLength=255)
     */
	private string $password;

	/**
	 * @ORM\Column(type="datetime")
	 * @Serializer\Expose()
	 * @Serializer\Groups({"usershow"})
	 */
	private ?DateTimeInterface $createdAt;

	/**
	 * @ORM\Column(type="datetime")
	 * @Serializer\Expose()
	 * @Serializer\Groups({"usershow"})
	 * @OA\Property(type="datetime")
	 */
	private ?DateTimeInterface $updatedAt;

	public function __construct()
	{
		$this->createdAt = new DateTime();
		$this->updatedAt = new DateTime();
		$this->uuid = $this->generateToken();
	}

	/**
	 * @param int $length
	 * @return bool|string
	 */
	private function generateToken($length = 128)
	{
		$hash = "";
		while(strlen($hash) < $length){
			try {
				$hash .= hash('sha512', random_bytes($length));
			} catch (Exception $e) {
				$hash .= hash('sha512', mt_rand(0,$length*10000));
			}
		}
		return substr($hash, 0,$length);
	}

	public function regenerateUuid()
	{
		$this->uuid = $this->generateToken();
	}

    public function getId(): ?int
    {
        return $this->id;
    }

	public function getCreatedAt(): DateTimeInterface
	{
		return $this->createdAt;
	}

	public function setCreatedAt(DateTimeInterface $createdAt): User
	{
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 * @return DateTimeInterface|null
	 */
	public function getUpdatedAt(): ?DateTimeInterface
	{
		return $this->updatedAt;
	}

	/**
	 * @param DateTimeInterface|null $updatedAt
	 * @return User
	 */
	public function setUpdatedAt(?DateTimeInterface $updatedAt): User
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}



	public function getUuid() : ?string
	{
		return $this->uuid;
	}

	public function setUuid(string $uuid) : User
	{
		$this->uuid = $uuid;
		return $this;
	}

	/**
	 * @return Company
	 */
	public function getCompany(): Company
	{
		return $this->company;
	}

	/**
	 * @param Company $company
	 * @return User
	 */
	public function setCompany(Company $company): User
	{
		$this->company = $company;
		return $this;
	}


	public function getFirstname(): ?string
	{
		return $this->firstname;
	}

	public function setFirstname(?string $firstname): User
	{
		$this->firstname = $firstname;
		return $this;
	}

	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function setLastname(?string $lastname): User
	{
		$this->lastname = $lastname;
		return $this;
	}

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

	public function setUsername(string $username): self
	{
		$this->username = $username;

		return $this;
	}


    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

	/**
	 * @param array $roles
	 * @return $this
	 */
    public function setRoles(array $roles): self
    {
    	foreach ($roles as $role){
    		if(!in_array($role,$this->roles)) {
			    $this->roles[] = $role;
		    }
	    }
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
