<?php

namespace App\Entity;

use App\Repository\PhoneRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use OpenApi\Annotations as OA;

/**
 * @ORM\Entity(repositoryClass=PhoneRepository::class)
 * @Serializer\ExclusionPolicy("all")
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "phone",
 *          parameters = { "phone" = "expr(object.getId())" },
 *          absolute = true
 *     ),
 *     exclusion= @Hateoas\Exclusion(groups={"phonelist","phoneshow"}))
 * @Hateoas\Relation(
 *      "phones",
 *      href = @Hateoas\Route(
 *          "phones",
 *          absolute=true
 *      ),
 *     exclusion= @Hateoas\Exclusion(groups={"phonelist","phoneshow"})
 * )
 * @OA\Schema(
 *     description="Phone",
 *     type="object",
 *     title="Phone"
 * )
 */
class Phone
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"phonelist","phoneshow"})
     * @OA\Property(type="int64",example="1")
     */
	private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"phonelist","phoneshow"})
     * @OA\Property(type="string",example="BILMO 505NZ15 1")
     */
	private ?string $name;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"phonelist","phoneshow"})
     * @OA\Property(type="integer",example=1250)
     */
	private ?int $price;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"phoneshow"})
     * @OA\Property(type="integer",example=300)
     */
	private ?int $weight;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"phoneshow"})
     * @OA\Property(type="string",example="Le meilleur du Smartphone pour votre entreprise")
     */
	private ?string $description;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Expose()
     * @Serializer\Groups({"phoneshow"})
     * @OA\Property(type="datetime")
     */
	private ?DateTimeInterface $createdAt;

	/**
     * @ORM\Column(type="datetime")
     * @Serializer\Expose()
     * @Serializer\Groups({"phoneshow"})
     * @OA\Property(type="datetime")
     */
	private ?DateTimeInterface $updatedAt;


	public function __construct()
	{
		$this->createdAt = new DateTime();
	}

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

	/**
	 * @return DateTimeInterface|null
	 */
	public function getUpdatedAt(): ?DateTimeInterface
	{
		return $this->updatedAt;
	}

	/**
	 * @param DateTimeInterface|null $updatedAt
	 * @return Phone
	 */
	public function setUpdatedAt(?DateTimeInterface $updatedAt): Phone
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}

}
