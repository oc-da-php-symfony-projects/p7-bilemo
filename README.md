# P7 Bilmo 
## A simple API made with Symfony framework

The goal of this scholar project is to expose a web service for Bilmo, a fictitious phone manufacturer.

The company needs customers to access data in Json format from anywhere with an authentication token.

### Prerequisite

For install, you will need (presuming this is a local Windows 10 install):

*   [Wamp](https://www.wampserver.com/) (if Debian/Ubuntu you will need [Apache](https://httpd.apache.org/), [mariaDB](https://mariadb.org/) and [phpMyAdmin](https://www.phpmyadmin.net/))
*   [Composer](https://getcomposer.org/).
*   [Symfony Cli](https://symfony.com/download)

If you go local you would certainly love to use the symfony server to test project by running `symfony serve` . This command uses the Symfony Cli

### Installation and Configuration

1.   Download zip or clone repository from [Gitlab](https://gitlab.com/oc-da-php-symfony-projects/p7-bilemo)
2.   Create a database with phpMyAdmin or whatever you like
3.   Enter your database configuration in .env file at project's root. Don't forget to enter DB server version.
4.   Set up your local and distant server url in .env file SERVER variables for NelmioApiBundle to retrieve it in its settings.
5.   Update the SuperAdmin Credentials with yours in .env file SUPER_ADMIN variables, so the fixtures can create you a Super Admin account.
6.   Run `composer install` to get the dependencies installed. Database migration will be created,then migrated, and you'll get some demo fixtures loaded too.

    This last one works only if you have composer installed globally, if not there is a composer.phar file at project's root you can use too by running `php composer.phar install` .
    If you missed something and try to install again, in order to avoid errors, please make sure that:

    1. all tables in database are suppressed
    2. migrations directory is empty
7. Generate your JWT key pair by running this from your root project directory

Windows:

`ssh-keygen -t rsa -P "" -b 4096 -m PEM -f config/jwt/jwtRS256.key`  

`ssh-keygen -e -m PEM -f config/jwt/jwtRS256.key > config/jwt/jwtRS256.key.pub`

Linux:

`openssl genrsa -out config/jwt/private.pem -aes256 4096`

`openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`
8. Ensure your JWT token path variables and pass phrase are properly configured in .env file.

You're done!

### Usage

Go to your-api.com and follow the link to the doc you need.

Generate a token with super admin credentials you've updated and authenticate.

You can also define your api in Postman by getting the json file at [this address](https://bilmo.ananzebatanga.com/api/v1/doc.json/bilmo)

1. Select API tab in Postman.
2. Click "create new API"
3. Select Define Tab in API Section
4. Load the doc.json file 
5. Click "Generate Collection"

Start consume!  [Demo here](https://www.bilmo.ananzebatanga.com)

 [![Codacy Badge](https://app.codacy.com/project/badge/Grade/db4e49573bca41928f72f2f2c1e327c8)](https://www.codacy.com/gl/oc-da-php-symfony-projects/p7-bilemo/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=oc-da-php-symfony-projects/p7-bilemo&amp;utm_campaign=Badge_Grade)
### Thanks

Thanks to [OpenClassrooms](https://openclassrooms.com/fr/) for giving anybody, anywhere, some learning opportunities.

Thanks to my Mentor Thomas Gérault for his kind but demanding leading.

And many thanks to my Boss at work for letting me go back to School!

Let me learn more!

Love it!
